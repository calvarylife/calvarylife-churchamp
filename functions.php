<?php

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

function symbolset_script() {
	wp_enqueue_script( 'ss_social_script', get_stylesheet_directory_uri() . '/assets/fonts/symbolset/ss-social.js', array('jquery'), '', true );
	wp_enqueue_style( 'ss_social_style', get_stylesheet_directory_uri() . '/assets/fonts/symbolset/ss-social.css', array(), '1.0', 'screen,projection');
	wp_enqueue_script( 'ss_pika_script', get_stylesheet_directory_uri() . '/assets/fonts/symbolset/ss-pika.js', array('jquery'), '', true );
	wp_enqueue_style( 'ss_pika_style', get_stylesheet_directory_uri() . '/assets/fonts/symbolset/ss-pika.css', array(), '1.0', 'screen,projection');
}
add_action('wp_enqueue_scripts', 'symbolset_script');

// Remove default WordStrap Nav Menus + Add Child Theme Nav Menus
function ws_childtheme_register_nav() {
	unregister_nav_menu( 'primary_navigation' );
	unregister_nav_menu( 'footer_navigation' );
	register_nav_menus(
		array(
		'secondary_navigation_left' 	=> __( 'Secondary Navigation Left' ),
		'primary_navigation_left' 	=> __( 'Primary Navigation Left' ),
		'primary_navigation_right' 	=> __( 'Primary Navigation Right' ),
		'secondary_navigation_right' 	=> __( 'Secondary Navigation Right' ),
		)
	);
}
add_action( 'init', 'ws_childtheme_register_nav' );