<div class="btn-group">
	<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
		Name
		<span class="caret"></span>
	</a>
	<ul class="dropdown-menu text-left">
		<?php
		$byname_args = array( 'post_type' => 'missionaries', 'posts_per_page' => -1, 'order' => 'ASC' );
		$byname_query = new WP_Query ( $byname_args );
		while ( $byname_query->have_posts() ) : $byname_query->the_post(); { ?>
			<li><a href="/missions/missionaries/<?php the_permalink(); ?>" title="View Missionary Profile for <?php the_title(); ?>"><?php the_title(); ?></a></li>
		<?php } endwhile; ?>				
	</ul>
</div><!-- /.btn-group -->
<div class="btn-group">
	<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
		Country
		<span class="caret"></span>
	</a>
	<ul class="dropdown-menu pull-right text-right">
		<?php
		$bycountry = get_terms( 'missioncountry', 'orderby=name&order=ASC&hide_empty=1' );
		foreach ( $bycountry as $ddcountry ) {
			echo '<li><a href="/missions/missionaries/country/'.$ddcountry->slug.'" title="View Missionaries Serving in' .$ddcountry->name.'">'.$ddcountry->name.'</a></li>';
		} ?>				
	</ul>
</div><!-- /.btn-group -->
<div class="btn-group">
	<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
		Agency
		<span class="caret"></span>
	</a>
	<ul class="dropdown-menu pull-right text-right">
		<?php
		$byagency = get_terms( 'missionagency', 'orderby=name&order=ASC&hide_empty=1' );
		foreach ( $byagency as $ddagency ) {
			echo '<li><a href="/missions/missionaries/agency/'.$ddagency->slug.'" title="View Missionaries Serving in' .$ddagency->name.'">'.$ddagency->name.'</a></li>';
		} ?>				
	</ul>
</div><!-- /.btn-group -->