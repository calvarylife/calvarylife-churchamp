<div class="btn-group">
	<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
		Series
		<span class="caret"></span>
	</a>
	<ul class="dropdown-menu text-left">
		<?php
		$byseries = get_terms( 'sermonseries', 'orderby=date&order=DESC&hide_empty=1' );
		foreach ( $byseries as $ddseries ) {
			echo '<li><a href="/sermons/series/'.$ddseries->slug.'" title="View Sermons in the Series: '.$ddseries->name.'">'.$ddseries->name.'</a></li>';
		} ?>				
	</ul>
</div><!-- /.btn-group -->
<div class="btn-group">
	<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
		Speaker
		<span class="caret"></span>
	</a>
	<ul class="dropdown-menu pull-right text-right">
		<?php
		$byspeaker = get_terms( 'sermonspeaker', 'orderby=count&order=DESC&hide_empty=1' );
		foreach ( $byspeaker as $ddspeaker ) {
			echo '<li><a href="/sermons/speaker/'.$ddspeaker->slug.'" title="View Sermons Preached by: '.$ddspeaker->name.'">'.$ddspeaker->name.'</a></li>';
		} ?>				
	</ul>
</div><!-- /.btn-group -->
<div class="btn-group">
	<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
		Scripture
		<span class="caret"></span>
	</a>
	<ul class="dropdown-menu pull-right text-right">
		<?php
		$byscripture = get_terms( 'sermonscripture', 'orderby=count&order=DESC&hide_empty=1' );
		foreach ( $byscripture as $ddscripture ) {
			echo '<li><a href="/sermons/scripture/'.$ddscripture->slug.'" title="View Sermons in the Book of the Bible: '.$ddscripture->name.'">'.$ddscripture->name.'</a></li>';
		} ?>				
	</ul>
</div><!-- /.btn-group -->