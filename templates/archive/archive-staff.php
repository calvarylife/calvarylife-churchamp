<?php get_template_part('templates/structure/header'); ?>

<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title">Staff Directory</span><br>
					<span class="feature-subtitle">Meet our Ministry &amp; Support Teams</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-search span4 visible-desktop">
				<?php get_template_part('templates/meta/searchform'); ?>
			</div><!-- /.feature-search -->
			<div class="feature-link span4 visible-desktop">
				Click on photo for full profile
			</div><!-- /.feature-link -->
		<?php tha_feature_bottom(); ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span12'); ?> role="main">
	<div class="main-inner">

		<h2>Pastor/Director Ministry Team</h2><br>
		<?php
		$ws_grid_columns_staff = 4;
		$ws_span_size_staff = ws_grid_class( $ws_grid_columns_staff );
		$posts_per_page = -1;
		$query_string = array(
		'post_type' => 'staff',
		'tax_query' => array( array( 'taxonomy' => 'staffrole', 'field' => 'slug', 'terms' => array( 'pastor', 'director' ) ) ),
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
		);
		query_posts( $query_string );
		$ws_item_counter = 1;
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				if( $ws_item_counter == 1 ) ws_open_row();
				get_template_part( 'templates/grids/grid-staff' );
				if( $ws_item_counter % $ws_grid_columns_staff == 0 ) ws_close_row();
				if( $ws_item_counter % $ws_grid_columns_staff == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
				$ws_item_counter++;
			}
			if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
		} else {
			echo '<p>Apologies, but there are no staff members to display.</p>';
		}
		?>

		<hr />

		<h2>Support Team</h2><br>
		<?php
		$ws_grid_columns_staff = 4;

		$ws_span_size_staff = ws_grid_class( $ws_grid_columns_staff );
		$posts_per_page = -1;
		$query_string = array(
		'post_type' => 'staff',
		'staffrole' => 'support',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
		);
		query_posts( $query_string );
		$ws_item_counter = 1;
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				if( $ws_item_counter == 1 ) ws_open_row();
				get_template_part( 'templates/grids/grid-staff' );
				if( $ws_item_counter % $ws_grid_columns_staff == 0 ) ws_close_row();
				if( $ws_item_counter % $ws_grid_columns_staff == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
				$ws_item_counter++;
			}
			if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
		} else {
			echo '<p>Apologies, but there are no posts to display.</p>';
		}
		?>

	</div><!-- /.main-inner -->
</div><!-- /.main -->

<?php get_template_part('templates/structure/content-bottom-after'); ?>

<?php get_template_part('templates/structure/footer'); ?>