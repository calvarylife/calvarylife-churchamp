<?php get_template_part('templates/structure/header'); ?>

<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title">Missionary Directory</span><br>
					<span class="feature-subtitle">Learn about who we support</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-menu span4 visible-desktop">
				<span class="feature-subtitle">Find a Missionary by&hellip;</span><br>
				<?php get_template_part('templates/menus/menu-missionaries'); ?>
			</div><!-- /.feature-menu -->
		<?php tha_feature_bottom(); ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span12'); ?> role="main">
	<div class="main-inner">

		<div class="row hidden-desktop">

			<div class="span12 feature-menu">
				<span class="feature-subtitle">Find a Missionary by&hellip;</span><br class="visible-phone"> <?php get_template_part('templates/menus/menu-missionaries'); ?>
			</div><!-- /.feature-menu -->

		</div><!-- /.row -->

		<hr class="hidden-desktop">

		<h3>Click on a Missionary Photo to View the Full Profile</h3>
		<hr>
		<?php
		$ws_grid_columns_md = 4;
		$ws_span_size_md = ws_grid_class( $ws_grid_columns_md );
		$posts_per_page = -1;
		$query_string = array(
		'post_type' => 'missionaries',
		'posts_per_page' => -1,
		'orderby' => 'title',
		'order' => 'ASC'
		);
		query_posts( $query_string );
		$ws_item_counter = 1;
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				if( $ws_item_counter == 1 ) ws_open_row();
				get_template_part( 'templates/grids/grid-missionaries' );
				if( $ws_item_counter % $ws_grid_columns_md == 0 ) ws_close_row();
				if( $ws_item_counter % $ws_grid_columns_md == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
				$ws_item_counter++;
			}
			if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
		} else {
			echo '<p>Apologies, but there are no missionaries to display.</p>';
		}
		?>

	</div><!-- /.main-inner -->
</div><!-- /.main -->

<?php get_template_part('templates/structure/content-bottom-after'); ?>

<?php get_template_part('templates/structure/footer'); ?>