<?php get_template_part('templates/structure/header'); ?>

<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title">Ministry Directory</span><br>
					<span class="feature-subtitle">Learn about our groups and programs</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-search span4 visible-desktop">
				<?php get_template_part('templates/meta/searchform'); ?>
			</div><!-- /.feature-search -->
		<?php tha_feature_bottom(); ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span12'); ?> role="main">
	<div class="main-inner">

		<h3>Click on a Ministry title to view its Full Profile</h3><br>
		<?php
		$ws_grid_columns_md = 4;
		$ws_span_size_md = ws_grid_class( $ws_grid_columns_md );
		$posts_per_page = -1;
		$query_string = array(
		'post_type' => 'ministries',
		'posts_per_page' => -1,
		'orderby' => 'menu_order',
		'order' => 'ASC'
		);
		query_posts( $query_string );
		$ws_item_counter = 1;
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				if( $ws_item_counter == 1 ) ws_open_row();
				get_template_part( 'templates/grids/grid-ministries' );
				if( $ws_item_counter % $ws_grid_columns_md == 0 ) ws_close_row();
				if( $ws_item_counter % $ws_grid_columns_md == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
				$ws_item_counter++;
			}
			if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
		} else {
			echo '<p>Apologies, but there are no ministries to display.</p>';
		}
		?>

	</div><!-- /.main-inner -->
</div><!-- /.main -->

<?php get_template_part('templates/structure/content-bottom-after'); ?>

<?php get_template_part('templates/structure/footer'); ?>