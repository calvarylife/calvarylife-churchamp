<?php get_template_part('templates/structure/header'); ?>

<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title">Sermon Directory</span><br>
					<span class="feature-subtitle">Our Sunday Message Archive</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-menu span4 visible-desktop">
				<span class="feature-subtitle">Find a Sermon by&hellip;</span><br>
				<?php get_template_part('templates/menus/menu-sermons'); ?>
			</div><!-- /.feature-menu -->
		<?php tha_feature_bottom(); ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span12'); ?> role="main">
	<div class="main-inner">

		<div class="row hidden-desktop">

			<div class="span12 feature-menu">
				<span class="feature-subtitle">Find a Sermon by&hellip;</span><br class="visible-phone"> <?php get_template_part('templates/menus/menu-sermons'); ?>
			</div><!-- /.feature-menu -->

		</div><!-- /.row -->

		<hr class="hidden-desktop">

		<?php // requires Cagtegory Order and Taxonomy Terms Order plugin @source: http://wordpress.org/extend/plugins/taxonomy-terms-order/
		$args_latest = array( 'orderby' => 'term_order', 'order' => 'DESC', 'number' => 1 );
		$tax_sermon_series_latest = get_terms( 'sermonseries', $args_latest ); foreach ($tax_sermon_series_latest as $series) {
		?>

			<?php
			$series_link = '<a href="/sermons/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
			?>

			<?php
			$ss = $series->slug;
			$ss_l_query_string = array(
			'sermonseries' => $ss,
			'posts_per_archive_page' => 1,
			'orderby' => 'date',
			'order' => 'DESC'
			);
			$ss_latest = new WP_Query( $ss_l_query_string );
			while ($ss_latest->have_posts()) : $ss_latest->the_post(); $do_not_duplicate = $post->ID; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div class="row">

	<div class="span12">

		<div class="row">

			<div class="sermon-latest-heading span4">
				<h2>Latest Sermon</h2>
				<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				&#40;<?php the_time('M d, Y'); ?>&#41;<br> <?php the_title(); ?>
				</a></h3>
			</div>

					<hr class="visible-phone">

			<div class="sermon-detail span4">
				<h3><i class="icon-bookmark"></i>&nbsp; Sermon Details</h3>
				<?php if (function_exists('the_subtitle')) {
					if ( get_post_meta($post->ID, 'endvr_feature_subtitle', true) ) { ?>
						<div>
							<span class="prefix scripture">Sub-Title :</span>
							<?php the_subtitle(); ?>
						</div>
					<?php } else { }
				} ?>
				<div>
					<span class="prefix series">Series :</span>
					<?php $tax_sermon_series = wp_get_post_terms( $post->ID, 'sermonseries' ); foreach ($tax_sermon_series as $series) {
						$series_link = '<a href="/sermons/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
						echo $series_link;
					} ?>
				</div>
				<div>
					<span class="prefix">Speaker :</span>
					<?php $tax_sermon_speaker = wp_get_post_terms( $post->ID, 'sermonspeaker' ); foreach ($tax_sermon_speaker as $speaker) {
						$speaker_link = '<a href="/staff/' . $speaker->slug . '" title="' . sprintf(__('View profile page for this sermon speaker: %s', 'my_localization_domain'), $speaker->name) . '">' . $speaker->name . '</a>';
						echo $speaker_link;
					}  ?>
				</div>
				<?php if (get_field('_endvr_sermon_ref')) { ?>
					<div>
						<span class="prefix scripture">Scripture :</span>
						<?php the_field('_endvr_sermon_ref'); ?>
					</div>
				<?php } ?>
			</div><!-- /.sermon-detail -->

					<hr class="visible-phone">

			<div class="sermon-audio span4"> <!-- requires an Endeavr modified version of MediaElement.js plugin @source: http://mediaelementjs.com/ -->
				<h3><i class="icon-headphones"></i>&nbsp; Sermon Audio</h3>
					<?php
						$endvr_sermon_audio_id = get_field('_endvr_sermon_audio');
						$endvr_sermon_audio_src = wp_get_attachment_url( $endvr_sermon_audio_id );
					?>
				<?php if ( $endvr_sermon_audio_id ) { ?>
				<div class="sermon-external-link">
					<span class="prefix">Download :</span>

					&#40; <a href="<?php echo $endvr_sermon_audio_src; ?>" title="Download the Sermon Audio" target="_blank"><?php the_time('Y-m-d'); ?>.mp3</a> &#41;
				</div>
				<?php } ?>
				<?php if (get_field('_endvr_sermon_audio')) { ?>
					<div class="media audio">
						<?php echo do_shortcode('[audio src="'.$endvr_sermon_audio_src.'"]'); ?>
					</div><!-- /.media -->
				<?php } else {
					echo '<p>There is no audio file available for this sermon at this time.</p>';
				} ?>
				<!--
					Proper way to use medialement with ACF: http://support.advancedcustomfields.com/discussion/3195/acf-with-audio-player/p1
					Necessary edit to mediaelement plugin: http://wordpress.org/support/topic/undefined-variable-notices-after-update
				-->
			</div><!-- /.sermon-audio -->

		</div>

		<br>

					<hr class="visible-phone">

		<div class="row">

			<div class="sermon-doc span6"> <!-- requires Google Doc Embedder plugin @source: http://www.davistribe.org/gde/ -->
				<h3><i class="icon-edit"></i>&nbsp; Sermon Outline</h3>
					<?php
						$endvr_sermon_doc_id = get_field('_endvr_sermon_doc');
						$endvr_sermon_doc_src = wp_get_attachment_url( $endvr_sermon_doc_id );
					?>
				<?php if ( $endvr_sermon_doc_id ) { ?>
					<div class="media doc">
						<?php echo do_shortcode('[gview height="430px" file="'.$endvr_sermon_doc_src.'"]'); ?>
					</div><!-- /.media -->
				<?php } else {
					echo '<p>There is no outline document available for this sermon at this time.</p>';
				} ?>
			</div><!-- /.sermon-doc -->

					<hr class="visible-phone">

			<div class="sermon-video span6"> <!-- requires Responsive Video Shortcodes plugin @source: http://wordpress.org/extend/plugins/responsive-video-shortcodes/ -->
				<h3><i class="icon-facetime-video"></i>&nbsp; Sermon Video</h3>
				<?php if (get_field('_endvr_sermon_video')) { ?>
					<div class="media video">
						<?php
						$endvr_sermon_video_src = get_field('_endvr_sermon_video');
						echo do_shortcode('[video align="left" aspect_ratio="4:3" width="100"]'.$endvr_sermon_video_src.'[/video]');
						?>
					</div><!-- /.media -->
				<?php } else {
					echo '<p>There is no video available for this sermon at this time.</p>';
				} ?>
			</div><!-- /.sermon-video -->

		</div>

	</div>

</div><!-- /.row -->

</article><!-- #post-<?php the_ID(); ?> -->

			<?php endwhile; ?>

			<hr>

			<h2><?php echo $series_link; ?></h2><br>

			<?php
			$ss = $series->slug;
			$ws_grid_columns_ss = 3;
			$ws_span_size_ss = ws_grid_class( $ws_grid_columns_ss );
			$ss_lo_query_string = array(
			'sermonseries' => $ss,
			'posts_per_archive_page' => 100, // offset will not work unless this is a postive integer
			'orderby' => 'date',
			'order' => 'DESC',
			'offset' => 1
			);
			$ss_latest_offset = new WP_Query( $ss_lo_query_string );
			$ws_item_counter = 1;
			if ($ss_latest_offset->have_posts()) {
				while ($ss_latest_offset->have_posts()) {
					$ss_latest_offset->the_post(); $do_not_duplicate = $post->ID;
					if( $ws_item_counter == 1 ) ws_open_row();
					get_template_part( 'templates/grids/grid-sermons' );
					if( $ws_item_counter % $ws_grid_columns_ss == 0 ) ws_close_row();
					if( $ws_item_counter % $ws_grid_columns_ss == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
					$ws_item_counter++;
				}
				if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
			} else {
				echo '<p>Apologies, but there are no sermons to display.</p>';
			}
			?>

		<?php } ?>

		<?php // requires Cagtegory Order and Taxonomy Terms Order plugin @source: http://wordpress.org/extend/plugins/taxonomy-terms-order/
		$series_latest = $series->term_id;
		$args_grid = array( 'orderby' => 'term_order', 'order' => 'DESC', 'number' => '100', 'exclude' => $series_latest ); // note the magic of exclude... offset did not work
		$tax_sermon_series_grid = get_terms( 'sermonseries', $args_grid ); foreach ($tax_sermon_series_grid as $series_grid) {
		?>

			<?php
			$series_grid_link = '<a href="/sermons/' . $series_grid->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series_grid->name) . '">' . $series_grid->name . '</a>';
			?>

			<hr>

			<h2><?php echo $series_grid_link; ?></h2><br>

			<?php
			$ss_g = $series_grid->slug;
			$ws_grid_columns_ss = 3;
			$ws_span_size_ss = ws_grid_class( $ws_grid_columns_ss );
			$ss_g_query_string = array(
			'sermonseries' => $ss_g,
			'posts_per_archive_page' => 100, // offset will not work unless this is a postive integer
			'orderby' => 'date',
			'order' => 'DESC'
			);
			$ss_grid = new WP_Query( $ss_g_query_string );
			$ws_item_counter = 1;
			if ($ss_grid->have_posts()) {
				while ($ss_grid->have_posts()) {
					$ss_grid->the_post(); $do_not_duplicate = $post->ID;
					if( $ws_item_counter == 1 ) ws_open_row();
					get_template_part( 'templates/grids/grid-sermons' );
					if( $ws_item_counter % $ws_grid_columns_ss == 0 ) ws_close_row();
					if( $ws_item_counter % $ws_grid_columns_ss == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
					$ws_item_counter++;
				}
				if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
			} else {
				echo '<p>Apologies, but there are no sermons to display.</p>';
			}
			?>

		<?php } ?>

	</div><!-- /.main-inner -->
</div><!-- /.main -->

<?php get_template_part('templates/structure/content-bottom-after'); ?>

<?php get_template_part('templates/structure/footer'); ?>