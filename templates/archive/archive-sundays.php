<?php get_template_part('templates/structure/header'); ?>

<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title">Sunday Event Schedule</span><br>
					<span class="feature-subtitle">Join us for Worship</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-search span4 visible-desktop">
				<?php get_template_part('templates/meta/searchform'); ?>
			</div><!-- /.feature-search -->
			<div class="feature-link span4 visible-desktop">
				Click on Event for More Info
			</div><!-- /.feature-link -->
		<?php tha_feature_bottom(); ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span8'); ?> role="main">
	<div class="main-inner">

		<?php
		$sun_index_args = array ( 'pagename' => 'sunday-schedule' );
		$sun_index = new WP_Query( $sun_index_args );
		while ( $sun_index->have_posts() ) : $sun_index->the_post();
		the_content();
		endwhile;
		wp_reset_postdata();
		?>

	</div><!-- /.main-inner -->
</div><!-- /.main -->

<?php tha_sidebars_before(); ?>
<aside id="sidebar" <?php ws_sidebar_class('span4'); ?> role="complementary">
	<div class="sidebar-inner">
		<?php tha_sidebar_top(); ?>

			<section id="endvr-widget-sunday-events" class="endvr-widget-sunday-events widget">
				<div class="widget-inner">

				<?php
				$sun_event_args = array ( 'post_type' => 'sundays', 'order_by' => 'menu_order', 'order' => 'ASC', 'posts_per_page' => -1, 'tax_query' => array (
					array (
						'taxonomy' => 'sundayevent',
						'field' => 'slug',
						'terms' => 'adult-sunday-school',
						'operator' => 'NOT IN',
					)
				) );
				$sun_event = new WP_Query( $sun_event_args );
				while ( $sun_event->have_posts() ) : $sun_event->the_post(); ?>

					<a class="gi-anchor well well-small" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<header class="gi-heading">
								<h3 class="gi-title"><?php the_title(); ?></h3>
								<?php if (function_exists('the_subtitle')) {
								if ( get_post_meta($post->ID, 'endvr_feature_subtitle', true) ) { ?>
									<span class="gi-tagline"><?php the_subtitle(); ?></span>
								<?php }
								} ?>
							</header>
						</article><!-- #post-<?php the_ID(); ?> -->
					</a><!-- end anchor -->

				<?php endwhile; wp_reset_postdata(); ?>

				<?php
				$sun_event_args = array ( 'post_type' => 'sundays', 'order_by' => 'menu_order', 'order' => 'ASC', 'sundayevent' => 'adult-sunday-school' );
				$sun_event = new WP_Query( $sun_event_args );
				while ( $sun_event->have_posts() ) : $sun_event->the_post(); ?>

					<a class="gi-anchor well well-small" href="/sundays/schedule/adult-sunday-school/" title="<?php the_title(); ?>">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<header class="gi-heading">
								<h3 class="gi-title"><?php the_title(); ?></h3>
								<?php if (function_exists('the_subtitle')) {
								if ( get_post_meta($post->ID, 'endvr_feature_subtitle', true) ) { ?>
									<span class="gi-tagline"><?php the_subtitle(); ?></span>
								<?php }
								} ?>
							</header>
						</article><!-- #post-<?php the_ID(); ?> -->
					</a><!-- end anchor -->

				<?php endwhile; ?>

				</div><!-- /.widget-inner -->
			</section><!-- /.endvr-widget-sunday-events -->

	     <?php tha_sidebar_bottom(); ?>
	</div><!-- /.sidebar-inner -->
</aside><!-- /.sidebar -->
<?php tha_sidebars_after(); ?>

<?php get_template_part('templates/structure/content-bottom-after'); ?>

<?php get_template_part('templates/structure/footer'); ?>