<?php tha_footer_before(); ?>
<footer class="sitefooter">
	<div id="colophon" <?php ws_colophon_class(); ?> role="contentinfo">
		<div class="container">
			<div class="row">
			<div class="span12">
				<div class="row">
					<div id="copyright" class="copyright span6">
						<p>&copy; 2013 Calvary Church of Santa Ana. All rights reserved.
						</p>
					</div><!-- /.copyright -->
					<div id ="credits" class="credits span6">
						<p>Powered by: <a href="http://endeavr.com" title="Endeavr">Endeavr</a>
					</div><!-- /.credits -->
				</div><!-- /.row -->
			</div><!-- /.span12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.colophon -->
</footer><!-- /.sitefooter -->
<?php tha_footer_after(); ?>

<script type="text/javascript">
$(document).ready(function() {
	$('.wait-till-load').show();
});
$('[data-spy="affix"]').affix('refresh');
</script>

<?php get_template_part('templates/javascript/javascript', 'footer'); ?>
<?php get_template_part('templates/javascript/js', 'mailchimp'); ?>

<?php wp_footer(); ?>