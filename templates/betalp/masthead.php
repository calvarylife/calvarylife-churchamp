<div id="masthead" <?php ws_masthead_class(); ?>>
	<div class="container">
	<div class="row">
	<div class="span12">
		<div class="row">
			<div id="brand" class="brand span4">
			      <a href="<?php echo home_url(); ?>/">
			        	<?php
			        		$ws_brand_logo = of_get_option('ws_brand_logo');
				   		$ws_brand_font_text = of_get_option('ws_brand_font_text');
			        	?>
				   	<img class="brand_logo" src="<?php echo $ws_brand_logo; ?>" alt="<?php echo $ws_brand_font_text; ?>" />
			      </a>
			</div><!-- /.brand -->
		</div><!-- /.row -->
	</div><!-- /.span12 -->
	</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.masthead -->