<?php get_template_part('templates/betalp/header'); ?>

<?php get_template_part('templates/betalp/feature'); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div class="span12">
<div class="betalp">

<h3>A brand new Calvary Church website is in the works!</h3>
<p>Fill in the form below to subscribe to the CalvaryLife Beta email list. You will receive <strong>exclusive</strong> access to the beta preview of the new CalvaryLife website.</p>
<p>We are actively developing the site and will open up a preview to beta testers as soon as possible. We'll send out follow up emails whenever we have something exciting to share about a new section of the site or we need your input on a particular feature.</p>
<hr>

<div class="betalp-mailchimp">
	<div id="mc_embed_signup">
	<form action="http://calvarylife.us7.list-manage.com/subscribe/post?u=216e20aecfc0101bb0549edff&amp;id=f51a48ab22" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
	<div class="mc-field-group">
		<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
	</label>
		<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
	</div>
	<div class="mc-field-group">
		<label for="mce-FNAME">First Name </label>
		<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
	</div>
	<div class="mc-field-group">
		<label for="mce-LNAME">Last Name </label>
		<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
	</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>
	<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-info"></div>
	</form>
	</div>
</div>

<hr>
<p>If you have any questions or comments along the way, please contact the Web Team within the Calvary Communications & Creative Arts ministry.</p>

</div>
</div>

<?php get_template_part('templates/structure/content-bottom-after'); ?>

<div class="betalp-contact">

	<!-- Mobile Wrapper -->
	<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse !important; border-color:transparent !important; border-width:0px !important; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
		<tbody><tr>
			<td width="100%" valign="top">

				<!-- Contact Info -->
				<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse !important; border-color:transparent !important; border-width:0px !important; mso-table-lspace:0pt; mso-table-rspace:0pt;">
					<tbody>
						<tr>
							<td width="100%" style="font-size: 15px; color: #664D2C; padding-center:20px; text-align: center; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 20px; letter-spacing:1px;">
								<center><span style="color:#664D2C;">1010 NORTH TUSTIN AVE<br>SANTA ANA, CA 92705</span></center>
							</td>
						</tr>
						<tr>
							<td width="100%" style="font-size: 15px; color: #664D2C; padding-center:20px; text-align: center; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 20px; letter-spacing:1px;">
								<center><span style="color:#664D2C;">+ 1 714 973 4800</span></center>
							</td>
						</tr>
						<tr>
							<td width="100%" height="40"></td>
						</tr>
						<tr>
							<td width="100%" style="font-size: 13px; color: #e32856; padding-center:20px; text-align: center; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; line-height: 15px; letter-spacing:3px;">
								<center><a href="mailto:webmaster@calvarylife.org" style="text-decoration: none; color: #664D2C;"><span style="color:#664D2C;">WEBMASTER@CALVARYLIFE.ORG</span></a></center>
							</td>
						</tr>
					</tbody>
				</table><!-- /END/ Contact -->

				<!-- Social -->
				<table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse !important; border-color:transparent !important; border-width:0px !important; mso-table-lspace:0pt; mso-table-rspace:0pt;">
					<tbody>
					<tr>
						<td width="auto" valign="top">

							<!-- Spacer -->
							<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse !important; border-color:transparent !important; border-width:0px !important; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
								<tbody><tr>
									<td width="100%" height="50"></td>
								</tr></tbody>
							</table><!-- /END/ Spacer -->

							<!-- Social Icon Set -->
							<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse !important; border-color:transparent !important; border-width:0px !important; mso-table-lspace:0pt; mso-table-rspace:0pt;">
								<tbody><tr>
									<td width="20%"></td>
									<td valign="middle" width="40" height="40" style="text-align: center;" class="iconScale">
										<!-- Social Icon -->
										<table width="40" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse !important; border-color:transparent !important; border-width:0px !important; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tbody><tr>
												<td valign="middle" height="40" style="text-align: center;">
													<center><a href="https://www.facebook.com/calvarylife"><img src="https://s3-us-west-1.amazonaws.com/calvarylife.email/assets/calvary-social-facebook.png" alt="CalvaryLife on Facebook" border="0" style="vertical-align: middle;"></a></center>
												</td>
											</tr></tbody>
										</table><!-- /END/ Social Icon -->
									</td>
									<td width="5%"></td>
									<td valign="middle" width="40" height="40" style="text-align: center;" class="iconScale">
										<!-- Social Icon -->
										<table width="40" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse !important; border-color:transparent !important; border-width:0px !important; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tbody><tr>
												<td valign="middle" height="40" style="text-align: center;">
													<center><a href="http://www.twitter.com/CalvaryLife"><img src="https://s3-us-west-1.amazonaws.com/calvarylife.email/assets/calvary-social-twitter.png" alt="CalvaryLife on Twitter" border="0" style="vertical-align: middle;"></a></center>
												</td>
											</tr></tbody>
										</table><!-- /END/ Social Icon -->
									</td>
									<td width="5%"></td>
									<td valign="middle" width="40" height="40" style="text-align: center;" class="iconScale">
										<!-- Social Icon -->
										<table width="40" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse !important; border-color:transparent !important; border-width:0px !important; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tbody><tr>
												<td valign="middle" height="40" style="text-align: center;">
													<center><a href="http://scribd.com/calvarylife"><img src="https://s3-us-west-1.amazonaws.com/calvarylife.email/assets/calvary-social-scribd.png" alt="CalvaryLife on Scribd" border="0" style="vertical-align: middle;"></a></center>
												</td>
											</tr></tbody>
										</table><!-- /END/ Social Icon -->
									</td>
									<td width="20%"></td>
								</tr></tbody>
							</table><!-- /END/ Social Icon Set -->

						</td>
					</tr></tbody>
				</table><!-- /END/ Social -->

			</td>
		</tr></tbody>
	</table><!-- /END/ Mobile Wrapper -->

</div>


<?php get_template_part('templates/betalp/footer'); ?>