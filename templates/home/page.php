<?php get_template_part('templates/home/header'); ?>

<?php get_template_part('templates/home/feature'); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div class="endvr-home-worship">
<div class="container">
<div class="row endvr-home-worship-block">
<div class="span12">

<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<p>This is paragraph</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at quam ornare, venenatis dolor non, sollicitudin ipsum. Etiam scelerisque est at nulla tristique pharetra. Maecenas mollis molestie volutpat. Mauris sit amet egestas risus. Donec at dui nunc. Sed in scelerisque odio. Nam accumsan, purus eget ultricies tempus, ligula risus scelerisque sapien, vitae hendrerit dolor neque a lectus.</p>

<p>Phasellus at egestas ante. Duis varius lectus in diam dictum, at tempus lacus varius. Donec tempus ligula eget ipsum luctus, in lobortis velit gravida. Vestibulum vulputate, lacus sit amet porttitor ullamcorper, enim lorem adipiscing tortor, sit amet ultricies ipsum justo eget orci. Phasellus nulla neque, ornare non commodo nec, ultrices a libero. Donec tincidunt metus quis blandit vulputate. Duis euismod viverra sapien vel dictum. Nulla sit amet commodo justo, ut elementum nibh. Etiam tempus dapibus nisl non gravida. Donec ultricies semper orci et venenatis. Integer luctus accumsan justo eu tristique.</p>

<p>Vestibulum eleifend massa erat, venenatis tincidunt ligula bibendum vel. In molestie et lacus nec cursus. Duis ut venenatis ligula. Ut at massa suscipit, dignissim dolor eu, sodales risus. Cras egestas, enim a tempus luctus, tellus libero euismod ante, at pharetra eros sapien quis arcu. Mauris orci dui, commodo nec venenatis eu, venenatis viverra felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi tristique dui eu arcu imperdiet vehicula. Phasellus cursus augue dui, nec auctor nibh pellentesque at.</p>

<p>Mauris non diam bibendum, convallis tortor a, dapibus enim. Phasellus ac purus eget mauris venenatis eleifend. Vestibulum nec ipsum ultrices urna laoreet interdum mollis et nibh. Praesent vel adipiscing nisi. Nunc pulvinar tellus et lorem gravida, id congue enim mattis. Aliquam vitae nisl placerat lorem placerat aliquet. Integer volutpat velit at dolor gravida, vel feugiat purus aliquet. Proin diam lorem, posuere sed pretium quis, venenatis auctor mi. Pellentesque accumsan tortor est, a ultrices elit tincidunt sit amet. Fusce lobortis nisi ut vehicula laoreet. Pellentesque blandit enim sit amet placerat cursus. Nunc elementum sem non metus porttitor dapibus. Nam at molestie enim. Proin eget est sit amet quam faucibus dapibus. Mauris pretium, est eu tincidunt varius, augue enim scelerisque sapien, quis ultricies elit nisl nec felis.</p>

<p>Nam vel quam enim. Vestibulum auctor, orci in egestas egestas, ante neque consectetur erat, facilisis molestie nisi lorem nec urna. Aliquam erat volutpat. Curabitur commodo lacinia tortor, eget sollicitudin orci varius nec. Donec magna risus, vulputate ac facilisis non, tincidunt ut magna. In hac habitasse platea dictumst. Morbi vitae lectus sagittis, commodo ipsum eget, accumsan eros. Quisque ullamcorper non ligula non tincidunt. Duis quis aliquam metus. Nulla facilisi.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at quam ornare, venenatis dolor non, sollicitudin ipsum. Etiam scelerisque est at nulla tristique pharetra. Maecenas mollis molestie volutpat. Mauris sit amet egestas risus. Donec at dui nunc. Sed in scelerisque odio. Nam accumsan, purus eget ultricies tempus, ligula risus scelerisque sapien, vitae hendrerit dolor neque a lectus.</p>

<p>Phasellus at egestas ante. Duis varius lectus in diam dictum, at tempus lacus varius. Donec tempus ligula eget ipsum luctus, in lobortis velit gravida. Vestibulum vulputate, lacus sit amet porttitor ullamcorper, enim lorem adipiscing tortor, sit amet ultricies ipsum justo eget orci. Phasellus nulla neque, ornare non commodo nec, ultrices a libero. Donec tincidunt metus quis blandit vulputate. Duis euismod viverra sapien vel dictum. Nulla sit amet commodo justo, ut elementum nibh. Etiam tempus dapibus nisl non gravida. Donec ultricies semper orci et venenatis. Integer luctus accumsan justo eu tristique.</p>

<p>Vestibulum eleifend massa erat, venenatis tincidunt ligula bibendum vel. In molestie et lacus nec cursus. Duis ut venenatis ligula. Ut at massa suscipit, dignissim dolor eu, sodales risus. Cras egestas, enim a tempus luctus, tellus libero euismod ante, at pharetra eros sapien quis arcu. Mauris orci dui, commodo nec venenatis eu, venenatis viverra felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi tristique dui eu arcu imperdiet vehicula. Phasellus cursus augue dui, nec auctor nibh pellentesque at.</p>

<p>Mauris non diam bibendum, convallis tortor a, dapibus enim. Phasellus ac purus eget mauris venenatis eleifend. Vestibulum nec ipsum ultrices urna laoreet interdum mollis et nibh. Praesent vel adipiscing nisi. Nunc pulvinar tellus et lorem gravida, id congue enim mattis. Aliquam vitae nisl placerat lorem placerat aliquet. Integer volutpat velit at dolor gravida, vel feugiat purus aliquet. Proin diam lorem, posuere sed pretium quis, venenatis auctor mi. Pellentesque accumsan tortor est, a ultrices elit tincidunt sit amet. Fusce lobortis nisi ut vehicula laoreet. Pellentesque blandit enim sit amet placerat cursus. Nunc elementum sem non metus porttitor dapibus. Nam at molestie enim. Proin eget est sit amet quam faucibus dapibus. Mauris pretium, est eu tincidunt varius, augue enim scelerisque sapien, quis ultricies elit nisl nec felis.</p>

<p>Nam vel quam enim. Vestibulum auctor, orci in egestas egestas, ante neque consectetur erat, facilisis molestie nisi lorem nec urna. Aliquam erat volutpat. Curabitur commodo lacinia tortor, eget sollicitudin orci varius nec. Donec magna risus, vulputate ac facilisis non, tincidunt ut magna. In hac habitasse platea dictumst. Morbi vitae lectus sagittis, commodo ipsum eget, accumsan eros. Quisque ullamcorper non ligula non tincidunt. Duis quis aliquam metus. Nulla facilisi.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at quam ornare, venenatis dolor non, sollicitudin ipsum. Etiam scelerisque est at nulla tristique pharetra. Maecenas mollis molestie volutpat. Mauris sit amet egestas risus. Donec at dui nunc. Sed in scelerisque odio. Nam accumsan, purus eget ultricies tempus, ligula risus scelerisque sapien, vitae hendrerit dolor neque a lectus.</p>

<p>Phasellus at egestas ante. Duis varius lectus in diam dictum, at tempus lacus varius. Donec tempus ligula eget ipsum luctus, in lobortis velit gravida. Vestibulum vulputate, lacus sit amet porttitor ullamcorper, enim lorem adipiscing tortor, sit amet ultricies ipsum justo eget orci. Phasellus nulla neque, ornare non commodo nec, ultrices a libero. Donec tincidunt metus quis blandit vulputate. Duis euismod viverra sapien vel dictum. Nulla sit amet commodo justo, ut elementum nibh. Etiam tempus dapibus nisl non gravida. Donec ultricies semper orci et venenatis. Integer luctus accumsan justo eu tristique.</p>

<p>Vestibulum eleifend massa erat, venenatis tincidunt ligula bibendum vel. In molestie et lacus nec cursus. Duis ut venenatis ligula. Ut at massa suscipit, dignissim dolor eu, sodales risus. Cras egestas, enim a tempus luctus, tellus libero euismod ante, at pharetra eros sapien quis arcu. Mauris orci dui, commodo nec venenatis eu, venenatis viverra felis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi tristique dui eu arcu imperdiet vehicula. Phasellus cursus augue dui, nec auctor nibh pellentesque at.</p>

<p>Mauris non diam bibendum, convallis tortor a, dapibus enim. Phasellus ac purus eget mauris venenatis eleifend. Vestibulum nec ipsum ultrices urna laoreet interdum mollis et nibh. Praesent vel adipiscing nisi. Nunc pulvinar tellus et lorem gravida, id congue enim mattis. Aliquam vitae nisl placerat lorem placerat aliquet. Integer volutpat velit at dolor gravida, vel feugiat purus aliquet. Proin diam lorem, posuere sed pretium quis, venenatis auctor mi. Pellentesque accumsan tortor est, a ultrices elit tincidunt sit amet. Fusce lobortis nisi ut vehicula laoreet. Pellentesque blandit enim sit amet placerat cursus. Nunc elementum sem non metus porttitor dapibus. Nam at molestie enim. Proin eget est sit amet quam faucibus dapibus. Mauris pretium, est eu tincidunt varius, augue enim scelerisque sapien, quis ultricies elit nisl nec felis.</p>

<p>Nam vel quam enim. Vestibulum auctor, orci in egestas egestas, ante neque consectetur erat, facilisis molestie nisi lorem nec urna. Aliquam erat volutpat. Curabitur commodo lacinia tortor, eget sollicitudin orci varius nec. Donec magna risus, vulputate ac facilisis non, tincidunt ut magna. In hac habitasse platea dictumst. Morbi vitae lectus sagittis, commodo ipsum eget, accumsan eros. Quisque ullamcorper non ligula non tincidunt. Duis quis aliquam metus. Nulla facilisi.</p>

</div>
</div>
</div>
</div>

<?php get_template_part('templates/structure/content-bottom-after'); ?>

<?php get_template_part('templates/home/footer'); ?>