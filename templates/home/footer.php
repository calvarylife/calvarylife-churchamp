<?php tha_footer_before(); ?>
<footer class="sitefooter">
	<div id="superfooter" <?php ws_footer_class(); ?> role="complementary">
		<?php tha_footer_top(); ?>
		<div class="container">
			<div class="row">
			<div class="span12">
				<div class="row">
					<div id="sf-one" class="sf-one sf-col span4">
						<?php dynamic_sidebar('sidebar-superfooter-col1'); ?>
					</div><!-- /.sf-one -->
					<div id="sf-two" class="sf-two sf-col span4">
						<?php dynamic_sidebar('sidebar-superfooter-col2'); ?>
					</div><!-- /.sf-two -->
					<div id="sf-three" class="sf-three sf-col span4">
						<?php dynamic_sidebar('sidebar-superfooter-col3'); ?>
					</div><!-- /.sf-three -->
				</div><!-- /.row -->
			</div><!-- /.span12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	  	<?php tha_footer_bottom(); ?>
	</div><!-- /.superfooter -->
	<div id="colophon" <?php ws_colophon_class(); ?> role="contentinfo">
		<div class="container">
			<div class="row">
			<div class="span12">
				<div class="row">
					<div id="copyright" class="copyright span6">
						<p>&copy; <?php echo date('Y'); ?>&nbsp;
							<?php $ws_brand_font_text = of_get_option( 'ws_brand_font_text' );
							if ( $ws_brand_font_text ) {
								echo $ws_brand_font_text;
							} else {
								echo bloginfo( 'name ' );
							} ?>
						</p>
					</div><!-- /.copyright -->
					<div id ="credits" class="credits span6">
						<p>Powered by: <a href="http://endeavr.com" title="Endeavr">Endeavr</a>
					</div><!-- /.credits -->
				</div><!-- /.row -->
			</div><!-- /.span12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.colophon -->
</footer><!-- /.sitefooter -->
<?php tha_footer_after(); ?>

<script type="text/javascript">
$(document).ready(function() {
	$('.wait-till-load').show();
});
$('[data-spy="affix"]').affix('refresh');
</script>

<?php get_template_part('templates/javascript/javascript', 'footer'); ?>

<?php wp_footer(); ?>