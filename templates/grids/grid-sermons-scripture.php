<?php
/**
 * The template used for displaying staff in a grid.
 */
$ws_grid_columns_ss = 3;
$ws_span_size_ss = ws_grid_class( $ws_grid_columns_ss );
?>
<div class="grid-item <?php echo $ws_span_size_ss; ?>">
	<a class="gi-anchor well" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="gi-heading">
				<?php if ( get_field('_endvr_sermon_ref') ) { ?>
					<h3 class="gi-title">&#40; <?php the_field('_endvr_sermon_ref'); ?> &#41;</h3>
				<?php } else { ?>
					<h3 class="gi-title">&nbsp;</h3>
				<?php } ?>
				<span class="prefix">Title: </span><span class="gi-tagline">
					<?php the_title(); ?>
					<?php if (function_exists('the_subtitle')) {
						if ( get_post_meta($post->ID, 'endvr_feature_subtitle', true) ) { ?>
							<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php the_subtitle(); ?>
						<?php }
					} ?>
				</span>
			</header>
			<div class="gi-details">	
				<span class="prefix">Series: </span><span class="gi-tagline"><?php $tax_sermon_series = wp_get_post_terms( $post->ID, 'sermonseries' ); foreach ($tax_sermon_series as $series) echo $series->name; ?></span><br>
				<span class="prefix">A Sermon by: </span>
				<span class="gi-speaker">
					<i><?php $tax_sermon_speaker = wp_get_post_terms( $post->ID, 'sermonspeaker' ); foreach ($tax_sermon_speaker as $speaker) echo $speaker->name; ?></i>
				</span>
			</div>
			<br>
			<div class="gi-date-bar">
				<span class="gi-date"><?php the_time('Y - M - d'); ?></span>
			</div>	
		</article><!-- #post-<?php the_ID(); ?> -->
	</a><!-- end anchor -->
</div><!-- .grid-item (end) -->