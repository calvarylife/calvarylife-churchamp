<?php
/**
 * The template used for displaying ministries in a grid.
 */
$ws_grid_columns_sd = 4;
$ws_span_size_sd = ws_grid_class( $ws_grid_columns_sd ); 
?>
<div class="grid-item <?php echo $ws_span_size_sd; ?>">
	<a class="gi-anchor well" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>					
			<!--<img class="gi-img" src="" alt="<?php the_title(); ?>">-->
			<header class="gi-heading">
				<h3 class="gi-title"><?php the_title(); ?></h3>
				<span class="gi-tagline">
					<?php if (function_exists('the_subtitle')) {
						if ( get_post_meta($post->ID, 'endvr_feature_subtitle', true) ) { ?>
							<?php the_subtitle(); ?>
						<?php }
					} ?>
				</span>
			</header>
		</article><!-- #post-<?php the_ID(); ?> -->
	</a><!-- end anchor -->
</div><!-- .grid-item (end) -->