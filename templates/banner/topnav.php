<div <?php ws_navbar_class(); ws_affix(); ?>>
	<div <?php ws_navbar_inner_class(); ?>>
		<div class="container">

			<a class="brand span4 offset4" href="<?php echo home_url(); ?>/">
		        	<?php
		        		$ws_brand_logo = of_get_option('ws_brand_logo');
			   		$ws_brand_font_text = of_get_option('ws_brand_font_text');
		        	?>
			   	<img class="brand-logo brand-logo-full" src="<?php echo $ws_brand_logo; ?>" alt="<?php echo $ws_brand_font_text; ?>" width="280" height="auto">
				<?php
					$ws_navbarbrandlogo = of_get_option('ws_navbarbrandlogo');
					$ws_brand_font_text = of_get_option('ws_brand_font_text');
				?>
			 	<img class="brand-logo brand-logo-mark" src="<?php echo $ws_navbarbrandlogo; ?>" alt="<?php echo $ws_brand_font_text; ?>" width="40" height="auto">
		     </a><!-- /.brand -->

		      <div class="mobile-navbar">
			      <div class="mobile-menu">
			      	<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
			      		<strong>MENU</strong>
			      		<i class="icon-list-ul"></i>
			      	</a><!-- /.btn-navbar -->
			      </div>
			      <div class="mobile-quicklinks">
			      	<a class="btn btn-navbar btn-search" data-toggle="collapse" data-target=".search-collapse">
			      		<i class="icon-search"></i>
			      	</a><!-- /.btn-search -->
			      </div>
		      </div><!-- /.mobile-navbar -->

			<nav class="nav-sec nav-collapse collapse" role="navigation">

				<div class="sec-nav alignleft">
					<?php
					if (has_nav_menu('secondary_navigation_left')) {
					wp_nav_menu(array( 'theme_location' => 'secondary_navigation_left', 'menu_class' => 'nav', 'link_before' => '<i class="ss-icon">&nbsp;</i><br><span>', 'link_after' => '</span>' ));
					}
					?>
				</div><!-- /.nav-collapse -->

				<div class="sec-nav alignright">
					<?php
					if (has_nav_menu('secondary_navigation_right')) {
					wp_nav_menu(array( 'theme_location' => 'secondary_navigation_right', 'menu_class' => 'nav', 'link_before' => '<i class="ss-icon">&nbsp;</i><br><span>', 'link_after' => '</span>' ));
					}
					?>
				</div><!-- /.nav-collapse -->

			</nav>

			<nav class="nav-main nav-collapse collapse" role="navigation">

				<div class="prime-nav alignleft">
					<?php
					if (has_nav_menu('primary_navigation_left')) {
					wp_nav_menu(array( 'theme_location' => 'primary_navigation_left', 'menu_class' => 'nav' ));
					}
					?>
				</div><!-- /.nav-collapse -->

				<div class="prime-nav alignright">
					<?php
					if (has_nav_menu('primary_navigation_right')) {
					wp_nav_menu(array( 'theme_location' => 'primary_navigation_right', 'menu_class' => 'nav' ));
					}
					?>
				</div><!-- /.nav-collapse -->

			</nav>


			<nav class="nav-main ql-collapse collapse search-collapse" role="navigation">
				<?php get_template_part('templates/meta/navbar-search'); ?>
			</nav><!-- /.search-collapse -->


		</div><!-- /.container -->
	</div><!-- /.navbar-inner -->
</div><!-- /.navbar -->