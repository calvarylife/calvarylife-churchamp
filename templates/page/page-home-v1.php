<?php get_template_part('templates/structure/header'); ?>

<?php tha_entry_before(); ?>

<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header text-center span12">
				<h1>
					<span class="feature-title">Welcome to the website of historic Prescott, Arizona's First Baptist Church</span>
				</h1>
			</div><!-- /.feature-header -->
		<?php tha_feature_bottom(); ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span12'); ?> role="main">
	<div class="main-inner">

<h2 class="text-center heading-underline heading-highlight">Join us on Sunday Mornings at one of our Worship Environments</h2>

		<?php tha_entry_top(); ?>
		<div class="entry-content">

<?php
$ws_grid_columns_sunevent = 4;
$ws_span_size_sunevent = ws_grid_class( $ws_grid_columns_sunevent );
$ss_g_query_string = array(
'sundayevent' => 'worship-environment',
'posts_per_archive_page' => 4, // offset will not work unless this is a postive integer
'orderby' => 'menu_order',
'order' => 'ASC'
);
$ss_grid = new WP_Query( $ss_g_query_string );
$ws_item_counter = 1;
if ($ss_grid->have_posts()) {
	while ($ss_grid->have_posts()) {
		$ss_grid->the_post(); $do_not_duplicate = $post->ID;
		if( $ws_item_counter == 1 ) ws_open_row();
		get_template_part( 'templates/grids/grid-sunday-event-home' );
		if( $ws_item_counter % $ws_grid_columns_sunevent == 0 ) ws_close_row();
		if( $ws_item_counter % $ws_grid_columns_sunevent == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
		$ws_item_counter++;
	}
	if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
} else {
	echo '<p>There are no Sunday Events in '.$sunevent->name.' sunevent.</p>';
}
?>

		<?php wp_reset_postdata(); ?>



<?php
$ss_l_query_string = array(
'post_type' => 'sermons',
'posts_per_archive_page' => 1,
'orderby' => 'date',
'order' => 'DESC'
);
$ss_latest = new WP_Query( $ss_l_query_string );
while ($ss_latest->have_posts()) : $ss_latest->the_post(); $do_not_duplicate = $post->ID; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<h2 class="text-center heading-underline heading-highlight">We put our Sermons online each week&hellip; Here's the latest:</h2>

		<div class="row">

			<div class="sermon-series-image span4">
				<div class="media-frame">
				<?php $tax_sermon_series = wp_get_post_terms( $post->ID, 'sermonseries' ); foreach ($tax_sermon_series as $series) {
					$series_id = $series->term_id;
					$series_img = get_field('_endvr_taxonomy_image_sermonseries','sermonseries_319'); ?>
					<img src="<?php echo $series_img; ?>">
				<?php } ?>
				</div>
			</div>

				<hr class="visible-phone">

			<div class="span4">

					<h3 class="sermon-title"><i class="icon-bookmark"></i>&nbsp;
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
					</h3>

				<div class="sermon-detail">
					<?php if (function_exists('the_subtitle')) {
						if ( get_post_meta($post->ID, 'endvr_feature_subtitle', true) ) { ?>
							<h4 class="sermon-subtitle">
								<?php the_subtitle(); ?>
							</h4>
						<?php } else { }
					} ?>
					<div>
						<i class="icon-folder-close"></i>&nbsp;
						<span class="prefix">Series :</span>
						<?php $tax_sermon_series = wp_get_post_terms( $post->ID, 'sermonseries' ); foreach ($tax_sermon_series as $series) {
							$series_link = '<a href="/sermons/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
							echo $series_link;
						} ?>
					</div>
					<div>
						<i class="icon-user"></i>&nbsp;
						<span class="prefix">Speaker :</span>
						<?php $tax_sermon_speaker = wp_get_post_terms( $post->ID, 'sermonspeaker' ); foreach ($tax_sermon_speaker as $speaker) {
							$speaker_link = '<a href="/staff/' . $speaker->slug . '" title="' . sprintf(__('View profile page for this sermon speaker: %s', 'my_localization_domain'), $speaker->name) . '">' . $speaker->name . '</a>';
							echo $speaker_link;
						}  ?>
					</div>
					<?php if (get_field('_endvr_sermon_ref')) { ?>
						<div>
							<i class="icon-book"></i>&nbsp;
							<span class="prefix">Scripture :</span>
							<?php the_field('_endvr_sermon_ref'); ?>
						</div>
					<?php } ?>
					<div>
						<i class="icon-calendar"></i>&nbsp;
						<span class="prefix">Date :</span>
						<?php the_time('M d, Y'); ?>
					</div>
				</div><!-- /.sermon-detail -->

					<hr class="visible-phone">



				<div class="sermon-audio hidden-tablet"> <!-- requires an Endeavr modified version of MediaElement.js plugin @source: http://mediaelementjs.com/ -->

						<?php
							$endvr_sermon_audio_id = get_field('_endvr_sermon_audio');
							$endvr_sermon_audio_src = wp_get_attachment_url( $endvr_sermon_audio_id );
						?>
					<?php if ( $endvr_sermon_audio_id ) { ?>
					<div class="sermon-external-link">
						<i class="icon-headphones"></i>&nbsp;
						<span class="prefix">Download :</span>

						&#40; <a href="<?php echo $endvr_sermon_audio_src; ?>" title="Download the Sermon Audio" target="_blank"><?php the_time('Y-m-d'); ?>.mp3</a> &#41;
					</div>
					<?php } ?>
					<?php if (get_field('_endvr_sermon_audio')) { ?>
						<div class="media audio">
							<?php echo do_shortcode('[audio src="'.$endvr_sermon_audio_src.'"]'); ?>
						</div><!-- /.media -->
					<?php } else {
						echo '<p>There is no audio file available for this sermon at this time.</p>';
					} ?>
					<!--
						Proper way to use medialement with ACF: http://support.advancedcustomfields.com/discussion/3195/acf-with-audio-player/p1
						Necessary edit to mediaelement plugin: http://wordpress.org/support/topic/undefined-variable-notices-after-update
					-->
				</div><!-- /.sermon-audio -->

			</div><!-- /.span4 -->

				<hr class="visible-phone">

			<div class="sermon-video span4"> <!-- requires Responsive Video Shortcodes plugin @source: http://wordpress.org/extend/plugins/responsive-video-shortcodes/ -->
				<?php if (get_field('_endvr_sermon_video')) { ?>
					<div class="media video media-frame">
						<?php
						$endvr_sermon_video_src = get_field('_endvr_sermon_video');
						echo do_shortcode('[video align="left" aspect_ratio="4:3" width="100"]'.$endvr_sermon_video_src.'[/video]');
						?>
					</div><!-- /.media -->
				<?php } else {
					echo '<p>There is no video available for this sermon at this time.</p>';
				} ?>
			</div><!-- /.sermon-video -->

		</div><!-- /.row -->



</article><!-- #post-<?php the_ID(); ?> -->

<?php endwhile; ?>

		</div>
		<?php tha_entry_bottom(); ?>

	</div><!-- /.main-inner -->
</div><!-- /.main -->



<?php get_template_part('templates/structure/content-bottom-after'); ?>

<?php tha_entry_after(); ?>


<?php get_template_part('templates/structure/footer'); ?>