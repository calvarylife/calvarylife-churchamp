<?php get_template_part('templates/structure/header'); ?>


<?php tha_entry_before(); ?>

<?php get_template_part('templates/structure/feature'); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span12'); ?> role="main">
	<div class="main-inner">

		<?php tha_entry_top(); ?>
		<div class="entry-content">
			<?php the_content(); ?>
			<iframe src="http://www.google.com/calendar/embed?src=pubcal%40fbcprescott.org&ctz=America/Los_Angeles" style="border: 0" width="100%" height="800" frameborder="0" scrolling="no"></iframe>
		</div>
		<?php tha_entry_bottom(); ?>

	</div><!-- /.main-inner -->
</div><!-- /.main -->



<?php get_template_part('templates/structure/content-bottom-after'); ?>

<?php tha_entry_after(); ?>


<?php get_template_part('templates/structure/footer'); ?>