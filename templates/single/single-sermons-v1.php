<?php get_template_part('templates/structure/header'); ?>

<?php while (have_posts()) : the_post(); ?>
<?php tha_entry_before(); ?>
<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title"><?php echo ws_title(); ?></span><br>
					<?php if (function_exists('the_subtitle')) {
						if ( get_post_meta($post->ID, 'endvr_feature_subtitle', true) ) { ?>
							<span class="feature-subtitle"><?php the_subtitle(); ?><br class="visible-phone"> &#40;<?php the_field('_endvr_sermon_ref'); ?>&#41;</span>
						<?php } else { ?>
							<span class="feature-subtitle"><?php the_field('_endvr_sermon_ref'); ?></span>
						<?php }
					} ?>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-menu span4 visible-desktop">
				<span class="feature-subtitle">Find a Sermon by&hellip;</span><br>
				<?php get_template_part('templates/menus/menu-sermons'); ?>
			</div><!-- /.feature-menu -->
		<?php tha_feature_bottom(); ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span12'); ?> role="main">
	<div class="main-inner">

		<div class="row hidden-desktop">

			<div class="span12 feature-menu">
				<span class="feature-subtitle">Find a Sermon by&hellip;</span><br class="visible-phone"> <?php get_template_part('templates/menus/menu-sermons'); ?>
			</div><!-- /.feature-menu -->

		</div><!-- /.row -->

		<hr class="hidden-desktop">

		<?php tha_entry_top(); ?>
		<div class="entry-content">

			<div class="row">

				<div class="sermon-detail span6">
					<h3><i class="icon-bookmark"></i>&nbsp; Sermon Details</h3>
					<div>
						<span class="prefix scripture">Title :</span>
						<?php echo ws_title(); ?>
					</div>
					<?php if (function_exists('the_subtitle')) {
						if ( get_post_meta($post->ID, 'endvr_feature_subtitle', true) ) { ?>
							<div>
								<span class="prefix scripture">Sub-Title :</span>
								<?php the_subtitle(); ?>
							</div>
						<?php } else { }
					} ?>
					<div>
						<span class="prefix">Date :</span>
						<?php the_time('Y - M - d'); ?>
					</div>
					<div>
						<span class="prefix series">Series :</span>
						<?php $tax_sermon_series = wp_get_post_terms( $post->ID, 'sermonseries' ); foreach ($tax_sermon_series as $series) {
							$series_link = '<a href="/sermons/series/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
							echo $series_link;
						} ?>
					</div>
					<div>
						<span class="prefix">Speaker :</span>
						<?php $tax_sermon_speaker = wp_get_post_terms( $post->ID, 'sermonspeaker' ); foreach ($tax_sermon_speaker as $speaker) {
							$speaker_link = '<a href="/sermons/speaker/' . $speaker->slug . '" title="' . sprintf(__('View profile page for this sermon speaker: %s', 'my_localization_domain'), $speaker->name) . '">' . $speaker->name . '</a>';
							echo $speaker_link;
						}  ?>
					</div>
					<?php if (get_field('_endvr_sermon_ref')) { ?>
						<div>
							<span class="prefix scripture">Scripture :</span>
							<?php the_field('_endvr_sermon_ref'); ?>
						</div>
					<?php } ?>
				</div><!-- /.sermon-detail -->

						<hr class="visible-phone">

				<div class="sermon-audio span6"> <!-- requires an Endeavr modified version of MediaElement.js plugin @source: http://mediaelementjs.com/ -->
					<h3><i class="icon-headphones"></i>&nbsp; Sermon Audio</h3>
						<?php
							$endvr_sermon_audio_id = get_field('_endvr_sermon_audio');
							$endvr_sermon_audio_src = wp_get_attachment_url( $endvr_sermon_audio_id );
						?>
					<?php if ( $endvr_sermon_audio_id ) { ?>
					<div class="sermon-external-link">
						<span class="prefix">Download :</span>
						Right click > "Save Link As..."<br>
						&#40; <a href="<?php echo $endvr_sermon_audio_src; ?>" title="Download the Sermon Audio" target="_blank"><?php the_time('Y-m-d'); ?>.mp3</a> &#41;
					</div>
					<?php } ?>
		<!--
					<div class="sermon-external-link">
						<span class="prefix">Podcast :</span>
						Subscribe @ Feedburner
						&#40;<a href="http://feedburner.com/fbcprescott" title="Subscribe to the Sermon Podcast @ Feedburner.com">http://feedburner.com/fbcprescott/</a>&#41;
					</div>
		-->
					<?php if (get_field('_endvr_sermon_audio')) { ?>
						<div class="media audio">
							<?php echo do_shortcode('[audio src="'.$endvr_sermon_audio_src.'"]'); ?>
						</div><!-- /.media -->
					<?php } else {
						echo '<p>There is no audio file available for this sermon at this time.</p>';
					} ?>
					<!--
						Proper way to use medialement with ACF: http://support.advancedcustomfields.com/discussion/3195/acf-with-audio-player/p1
						Necessary edit to mediaelement plugin: http://wordpress.org/support/topic/undefined-variable-notices-after-update
					-->
				</div><!-- /.sermon-audio -->

			</div><!-- /.row -->

			<hr>

				<div class="sermon-video"> <!-- requires Responsive Video Shortcodes plugin @source: http://wordpress.org/extend/plugins/responsive-video-shortcodes/ -->
					<h3><i class="icon-facetime-video"></i>&nbsp; Sermon Video</h3>
					<div class="sermon-external-link">
						<span class="prefix">Channel :</span>
						Go to the FBC Prescott Video Channel
						&#40;<a href="http://vimeo.com/fbcprescott" title="Go to the FBC Prescott Video Channel @ Vimeo" target="_blank">http://vimeo.com/fbcprescott/</a>&#41;
					</div>
					<div class="sermon-external-link">
						<span class="prefix">Video :</span>
						Watch this Sermon Video
						&#40;<a href="<?php the_field('_endvr_sermon_video'); ?>" title="Watch this sermon video @ Vimeo" target="_blank"><?php the_field('_endvr_sermon_video'); ?></a>&#41;
					</div>
					<?php if (get_field('_endvr_sermon_video')) { ?>
						<div class="media video">
							<?php
							$endvr_sermon_video_src = get_field('_endvr_sermon_video');
							echo do_shortcode('[video align="left" aspect_ratio="4:3" width="100"]'.$endvr_sermon_video_src.'[/video]');
							?>
						</div><!-- /.media -->
					<?php } else {
						echo '<p>There is no video available for this sermon at this time.</p>';
					} ?>
				</div><!-- /.sermon-video -->

			<hr>

				<div class="sermon-doc"> <!-- requires Google Doc Embedder plugin @source: http://www.davistribe.org/gde/ -->
					<h3><i class="icon-edit"></i>&nbsp; Sermon Outline</h3>
						<?php
							$endvr_sermon_doc_id = get_field('_endvr_sermon_doc');
							$endvr_sermon_doc_src = wp_get_attachment_url( $endvr_sermon_doc_id );
						?>
					<?php if ( $endvr_sermon_doc_id ) { ?>
					<div class="sermon-external-link">
						<span class="prefix">Download :</span>
						Right click > "Save Link As..."<br>
						&#40; <a href="<?php echo $endvr_sermon_doc_src; ?>" title="Download the Sermon Outline" target="_blank"><?php the_time('Y-m-d'); ?>.pdf</a> &#41;
					</div>
					<?php } ?>
					<?php if (get_field('_endvr_sermon_doc')) { ?>
						<div class="media doc">
							<?php echo do_shortcode('[gview file="'.$endvr_sermon_doc_src.'"]'); ?>
						</div><!-- /.media -->
					<?php } else {
						echo '<p>There is no outline document available for this sermon at this time.</p>';
					} ?>
				</div><!-- /.sermon-doc -->

			<hr>

				<div class="sermon-ref"> <!-- requires ESV Bible Shortcode for WordPress @source: http://wordpress.org/extend/plugins/esv-bible-shortcode-for-wordpress/ -->
						<h3><i class="icon-book"></i>&nbsp; Sermon Scripture</h3>
						<div class="sermon_external_link">
							<span class="prefix esv">Online Bible :</span>
							Read @ ESV
							&#40;<a href="http://esvbible.org" title="Read the Bible Online @ ESVBible.org">http://esvbible.org/</a>&#41;
						</div>
						<?php if (get_field('_endvr_sermon_ref')) {
							$endvr_sermon_ref = get_field('_endvr_sermon_ref');
							echo do_shortcode('[esv scripture="'.$endvr_sermon_ref.'" include_footnotes="true" include_footnote_links="true" include_headings="true" include_subheadings="true" include_audio_link="true"]');
						} ?>
				</div><!-- /.sermon-ref -->

		</div>
		<footer>
			 <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'ws'), 'after' => '</p></nav>')); ?>
		</footer>
		<?php tha_entry_bottom(); ?>

	</div><!-- /.main-inner -->
</div><!-- /.main -->

<?php get_template_part('templates/structure/content-bottom-after'); ?>

</article><!-- /article -->
<?php tha_entry_after(); ?>
<?php endwhile; ?>

<?php get_template_part('templates/structure/footer'); ?>